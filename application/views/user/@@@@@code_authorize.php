<?php
$customerDetails = $this->db->query("SELECT * FROM enquiry e where isactive = 1")->result();
$itemDetails = $this->db->query("SELECT * FROM items WHERE item_type_id = 2 and is_active = 1")->result();
$itemDetailsFebrics = $this->db->query("SELECT * FROM items WHERE item_type_id = 3 and is_active = 1")->result();
$patternList = $this->db->query("SELECT * FROM patterns WHERE is_active = 1")->result();
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<!-- Tell the browser to be responsive to screen width -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="author" content="">
	<!-- Favicon icon -->
	<link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url(); ?>assets/client_asstes/images/favicon.png">
	<title>SuperEditors || Home Page</title>
	<!-- Custom CSS -->
	<link href="<?php echo base_url(); ?>assets/client_asstes/css/style.css" rel="stylesheet">
	<!--============For Editor===========-->
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/client_asstes/css/lib/html5-editor/bootstrap-wysihtml5.css" />
	<!--============For Editor===========-->
	<!--============For Accordian===========-->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
	<!--============For Accordian===========-->
</head>
<body class="header-fix fix-sidebar">
<style>
	.form-control {
		height: 30px;
		font-size:11px;
	}
	select.form-control:not([size]):not([multiple]) {
		height: 30px;
		font-size: 11px;
	}
	.disabled-options {
		pointer-events: none;
		background: #ca25736e;
		border: 1px solid #ca25736e;
	}
	#loading-bar-spinner.spinner {
		left: 50%;
		margin-left: -20px;
		top: 50%;
		margin-top: -20px;
		position: absolute;
		z-index: 19 !important;
		animation: loading-bar-spinner 400ms linear infinite;
	}

	#loading-bar-spinner.spinner .spinner-icon {
		width: 40px;
		height: 40px;
		border:  solid 4px transparent;
		border-top-color:  #ca2573 !important;
		border-left-color: #ca2573 !important;
		border-radius: 50%;
	}

	@keyframes loading-bar-spinner {
		0%   { transform: rotate(0deg);   transform: rotate(0deg); }
		100% { transform: rotate(360deg); transform: rotate(360deg); }
	}
	.toast-content {
		margin-top: 55px!important;
		background: #ca2573;
	}
	.table>thead>tr>th,.table>tbody>tr>td {
		font-size: 80%;
		font-weight: 400;
	}
	div.hide-div {
		display: none;
	}
	input.form-control.hide-input-field {
		border: none;
		background: white;
	}
	input#gensQty,input#gensRate,input#ladiesRate,input#ladiesRate,input#itemName,input#febricsItem{
		pointer-events: none;
	}
	input#generatedStatus {
		height: 14px;
		margin-top: 5px;
	}
	.table>thead>tr>th, .table>tbody>tr>td{
		cursor: pointer;
	}
	table#example2555 {
		margin-top: 33px;
	}
</style>
<!-- Main wrapper  -->
<div id="main-wrapper">
	<!-- header header  -->
	<?php include("includes/header.php") ?>
	<!-- End header header -->
	<!-- Left Sidebar  -->
	<?php include("includes/sidenav.php") ?>
	<!-- End Left Sidebar  -->
	<!-- Page wrapper  -->
	<div class="page-wrapper">
		<!-- Bread crumb -->
		<div class="row page-titles">
			<div class="col-md-5 align-self-center">
				<h3 class="text-primary">Main List Updations</h3>
			</div>
			<div class="col-md-7 align-self-center">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
					<li class="breadcrumb-item active">Main List Updations</li>
				</ol>
			</div>
		</div>
		<!-- End Bread crumb -->
		<!-- Container fluid  -->
		<div class="container-fluid">
			<!-- Start Page Content -->
			<form method="post" action = "<?php echo base_url() ?>CodeGeneration/save_code_data" id="codegeneratedForm" enctype='multipart/form-data'>
				<section id="po_section" name="po_section">
					<button type="button" class="btn btn-sm btn-primary btn-block text-white" data-toggle="collapse" data-target="#customerform" >Main List Updation</button>
					<div id="customerform" class="collapse in">
						<div class="card">
							<div class="card-body">
								<center>
									<p style="color:green"><?php echo $this->session->flashdata('message') ?></p>
								</center>
								<h5 class="card-title">Main List Updation</h5>
								<div class="row">
									<div class="col-sm-12">
										<div class="row" style="border-style: ridge;">
											<input type="hidden" value="" id="codeGenerationsId" name="codeGenerationsId">
											<div class="col-sm-4">
												<small>Company Name</small>
												<select class="form-control " name="company_name"   id="company_name" required>
													<option value="">Company Name</option>
													<option value = "SuperEditors">SuperEditors</option>
													<option value = "MannaMenswear">MannaMenswear</option>
												</select>
											</div>

											<div class="col-sm-4">
												<small>Updations For</small>
												<select class="form-control " name="updations_for"   id="updationsFor" required>
													<option value="">Select updations for</option>
													<option value="Authorize Code">Authorize Code</option>
													<option value = "Updations For Ready">Updations For Ready</option>
													<option value = "Updations For Sign">Updations For Sign</option>
												</select>
											</div>

											<div class="col-sm-4">
												<small>Customer</small>
												<select class="form-control" name="customer" id="customer" required>
													<option value="">Select Customer</option>
													<?php foreach($customerDetails as $getcustomerDetails){ ?>
														<option value="<?php echo $getcustomerDetails->name ?>"
																data-credit-limit="<?=$getcustomerDetails->credit_limit?>"
																data-state="<?=$getcustomerDetails->state?>"
																data-customer-id="<?=$getcustomerDetails->enquiry_id?>"
																data-delivery-address="<?=$getcustomerDetails->shipping_address?>"
														><?php echo $getcustomerDetails->name ?></option>
													<?php } ?>
												</select>
											</div>

											<div class="col-sm-4">
												<small>Code Number</small>
												<select class="form-control" name="code_number" id="coderNumber" required>
													<option value="">Select Code Number</option>
												</select>
											</div>

											<div class="col-sm-4">
												<small>Entry Date</small>
												<input type="date" class="form-control" name="entry_date" id="entry_date" value="<?php echo date("Y-m-d") ?>" >
											</div>

											<div class="col-sm-4">
												<div style="margin-top: 25px;">
													<input type="checkbox" name="show_pending_status" > Show only pending students data.
												</div>
											</div>

											<div class="col-sm-4">
												<small>Upload Excel File For Import</small>
												<input type="file" class="form-control" name="import_file" id="import_file" value=""  >
											</div>

											<div class="col-sm-4" >
												<small>Remark</small>
												<input type="text" class="form-control" name="remark" id="remark" placeholder="Remark" >
											</div>

											<div class="col-sm-12 table-responsive">
												<table id="example2555" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
													<thead>
													<tr>
														<th><input type="checkbox"></th>
														<th>SR NO</th>
														<th>Unique Code</th>
														<th>Unique name</th>
														<th>Gender(Gens/Ladies)</th>
														<th>Authorizations</th>
														<th>ID/Roll No</th>
														<th>Cell No</th>
														<th>Stud Name</th>
														<th>Mens Tak</th>
														<th>Mens Taken</th>
													</tr>
													</thead>
													<tbody id="getCodeGenerationLeftBlock" >

													</tbody>
												</table>
											</div>


											<div class="col-sm-12" style="margin-bottom: 10px">
												<br>
												<button type="submit" class="btn btn-sm btn-primary text-white submit-btn" id="readDataFromExcel">Read Data From Excel</button>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
			</form>

			<!-- End Container fluid  -->
			<!-- footer -->
			<?php include("includes/footer.php") ?>
			<!-- End footer -->
		</div>
		<!-- End Page wrapper  -->
	</div>
	<!-- End Wrapper -->
	<!-- All Jquery -->


	<!-- Scripts -->
	<script src="<?php echo base_url(); ?>assets/client_asstes/js/lib/jquery/jquery.min.js"></script>
	<!-- Bootstrap tether Core JavaScript -->
	<script src="<?php echo base_url(); ?>assets/client_asstes/js/lib/bootstrap/js/popper.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/client_asstes/js/lib/bootstrap/js/bootstrap.min.js"></script>
	<!-- slimscrollbar scrollbar JavaScript -->
	<script src="<?php echo base_url(); ?>assets/client_asstes/js/jquery.slimscroll.js"></script>
	<!--Menu sidebar -->
	<script src="<?php echo base_url(); ?>assets/client_asstes/js/sidebarmenu.js"></script>
	<!--stickey kit -->
	<script src="<?php echo base_url(); ?>assets/client_asstes/js/lib/sticky-kit-master/dist/sticky-kit.min.js"></script>
	<!--Custom JavaScript -->
	<script src="<?php echo base_url(); ?>assets/client_asstes/js/lib/webticker/jquery.webticker.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/client_asstes/js/lib/peitychart/jquery.peity.min.js"></script>
	<!-- scripit init-->
	<script src="<?php echo base_url(); ?>assets/client_asstes/js/custom.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/client_asstes/js/dashboard-1.js"></script>
	<!-- Data Tables-->
	<script src="<?php echo base_url(); ?>assets/client_asstes/js/lib/datatables/datatables.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/client_asstes/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/client_asstes/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/client_asstes/js/lib/datatables/cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/client_asstes/js/lib/datatables/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/client_asstes/js/lib/datatables/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
	<script src="<?php echo base_url(); ?>assets/client_asstes/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/client_asstes/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/client_asstes/js/lib/datatables/datatables-init.js"></script>
	<!--text editor kit -->
	<script src="<?php echo base_url(); ?>assets/client_asstes/js/lib/html5-editor/wysihtml5-0.3.0.js"></script>
	<script src="<?php echo base_url(); ?>assets/client_asstes/js/lib/html5-editor/bootstrap-wysihtml5.js"></script>
	<script src="<?php echo base_url(); ?>assets/client_asstes/js/lib/html5-editor/wysihtml5-init.js"></script>
	<!-- Bootstrap Modal -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>


	<script>
		$( document ).ready(function() {
			var successImage    ='<?php echo base_url(); ?>assets/alert_image/success.svg';

			$(document).on('change', 'select#customer', function() {
				var customerId          =$('option:selected',this).data("customer-id");
				var customerName=$(this).val();

				var url = "<?php echo base_url('CodeGeneration/get_code_number'); ?>";

				var post_data = {
					'customerId': customerId,
					'customerName': customerName,
					'<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>'
				};

				$.ajax({
					url : url,
					type : 'POST',
					data: post_data,
					success : function(result)
					{
						var obj = JSON.parse(result);
						for(var i=0;i<obj.length;i++)
						{
							var textData=obj[i]['prefix']+' '+obj[i]['number_starts_form']+' - '+obj[i]['prefix']+' '+obj[i]['number_of_code_generations']
							$('select#coderNumber').append($('<option/>', {
								value: obj[i]['prefix'],
								text : textData,
								datastartUp : obj[i]['number_starts_form'],
								dataEndOFCode: obj[i]['number_of_code_generations'],
							}));
						}
					}
				});
			});

			$(document).on('change', 'select#coderNumber', function() {

				var startUp         	  =$('select#coderNumber').find(':selected').attr('datastartUp');
				var dataEndOFCode          =$('select#coderNumber').find(':selected').attr('dataEndOFCode');
				var prefix					=$(this).val();

				var startUpSecond=Number(startUp);
				var dataEndOFCodeSecond=Number(dataEndOFCode);

				$('tbody#getCodeGenerationLeftBlock').html('');


				var url = "<?php echo base_url('CodeGeneration/getGenerationsInfo'); ?>";

				var post_data = {
					'prefix': prefix,
					'startUp': startUp,
					'dataEndOFCode': dataEndOFCode,
					'<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>'
				};

				var html='';

				$.ajax({
					url : url,
					type : 'POST',
					data:post_data,
					success : function(result)
					{
						if (result){
							var obj = JSON.parse(result);

							$.each(obj, function (index,value) {
								var className='odd';
								if (index%2==0){
									className='even';
								}
								html +=`<tr role="row" class="`+className+`">
										 <td><input type="checkbox" id="singleCodeAuthorizations"></td>
										 <td class="sorting_1">`+index+`</td>
										<td>`+value.unique_code+`</td>
										<td>`+value.unique_name+`</td>
										<td>`+value.gender+`</td>
										<td>`+value.authorization+`</td>
										<td>`+value.roll_no+`</td>
										<td>`+value.cell_no+`</td>
										<td>`+value.stud_name+`</td>
										<td>`+value.mens_tak+`</td>
										<td>`+value.mens_taken+`</td>
									 </tr>`;
							});

							$('tbody#getCodeGenerationLeftBlock').html(html);

							$('#example2555').DataTable({
								"pagingType": "full_numbers",
								"searching": false,
								dom: 'Bfrtip',
								buttons: [
									'copy', 'csv', 'excel', 'pdf', 'print'
								]
							});

						}else{
							for (var i=startUpSecond;i<=dataEndOFCodeSecond;i++){
								console.log("CCC");
								var className='odd';
								if (i%2==0){
									className='even';
								}
								html +=`<tr role="row" class="`+className+`">
							 <td><input type="checkbox" id="singleCodeAuthorizations"></td>
							 <td class="sorting_1">`+i+`</td>
							<td>`+prefix+'-'+i+`</td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
                         </tr>`;

							}

							$('tbody#getCodeGenerationLeftBlock').html(html);

							$('#example2555').DataTable({
								"pagingType": "full_numbers",
								"searching": false,
								dom: 'Bfrtip',
								buttons: [
									'copy', 'csv', 'excel', 'pdf', 'print'
								]
							});
						}
					},error: function (){
						alert("Something went wrong")
					}
				});

			});
		});


		var successImage    ='<?php echo base_url(); ?>assets/alert_image/success.svg';
		$("form#codegeneratedForm").submit(function(e) {
			e.preventDefault();

			var companyName				=$('select#company_name').val();
			var updateFor				=$('select#updationsFor').val();
			var customerName			=$('select#customer').val();
			var startUp         	  	=$('select#coderNumber').find(':selected').attr('datastartUp');
			var dataEndOFCode          	=$('select#coderNumber').find(':selected').attr('dataEndOFCode');
			var prefix					=$('select#coderNumber').val();


			var xhr = new XMLHttpRequest()

			var url = "<?php echo base_url('CodeGeneration/importCode'); ?>";

			//var formData = new FormData();
			var formData = new FormData($('form')[0]);
			formData.append('file',xhr.file);

			formData.append("companyName",companyName);
			formData.append("updateFor",updateFor);
			formData.append("customerName",customerName);
			formData.append("startUp",startUp);
			formData.append("dataEndOFCode",dataEndOFCode);
			formData.append("prefix",prefix);

			$.ajax({
				url : url,
				processData: false,
				contentType: false,
				cache: false,
				type : 'POST',
				data: formData, // serializes the form's elements.
				success : function(result)
				{
					if (result==1){
						cuteToast({
							type: "success", // or 'info', 'error', 'warning'
							message: "Successfully Import data",
							timer: 5000,
							img:successImage,
						});

						setTimeout(function() {
						location.reload();
						}, 1000);

					}else{
						alert("Import file missing");
					}

				},error: function (){
					alert("Something went wrong")
				},
				beforeSend: function (xhr){
					$('button#readDataFromExcel').html("<span class='fa fa-spin fa-spinner'></span> Processing...");
				}
			});
		});
	</script>


</body>
</html>
