<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Patterns extends CI_Controller
{
public function index()
    {
        
    if ($this->session->userdata['role'] == 1){
            $this->load->view('user/patterns');
         }else{
              $this->load->view('basic/login');
         }
        
    }
    public function add_patterns()
      {
           
          if ($this->session->userdata['role'] == 1){
            $this->load->view('user/add_patterns');
          }else{
             $this->load->view('basic/login');
          }
          
      }
      
      public function save_patterns_data(){

          $data=array(
                      "pattern_name"=>$this->input->post('name'),
                      "pattern_type"=>$this->input->post('pattern_type'),
                      "is_active"=>1,
                      "created_by" =>$this->session->userdata['user_id'],
                      "created_date"=>date("Y-m-d")
                      );
               
       $this->load->model('Patterns_model');
        
        $response = $this->Patterns_model->save_patterns_data($data);
        
        if ($response == true)
            { 
                
                 $this->session->set_flashdata('pattern_message', 'Patterns Data Saved Successfully');
                 redirect('Patterns/add_patterns');
            }
            else
            {
                echo "Insert error !";
            }
          
        }
        public function edit_patterns(){
             if ($this->session->userdata['role'] == 1){
            $this->load->view('user/edit_pattern');
        }else{
            $this->load->view('basic/login');
        }
        }
        
      public function save_edit_pattern_data(){

          $data=array(
              "pattern_name"=>$this->input->post('name'),
              "pattern_type"=>$this->input->post('pattern_type'),
              "is_active"=>1,
              "created_by" =>$this->session->userdata['user_id'],
              "created_date"=>date("Y-m-d")
          );
                     
          $pattern_id = $this->input->post('pattern_id');
          
          $this->load->model('Patterns_model');
          $response = $this->Patterns_model->edit_patterns_data($data,$pattern_id);
        if ($response == true)
            {
                
                $this->session->set_flashdata('pattern_message', 'Pattern Data Updated Successfully');
                redirect('Patterns');
            }
            else
            {
                echo "Insert error !";
            }
          
      }
        public function delete_pattern(){
     $pattern_id=$this->input->get('pattern_id');
    // print_r($measureid);
     
          $data = [
            'is_active' => '0'
        ];
      $this->load->model('Patterns_model');
     
     $response = $this->Patterns_model->delete_patterns_data($data,$pattern_id);
     
          
       if($response == true)
       {
          
        $this->session->set_flashdata('pattern_message', 'Pattern data Deleted Successfully');
        redirect('Patterns');
               
       }
      else
        {
            echo "Insert error !";
            
            
        }
    
          
      }
      

}
?>