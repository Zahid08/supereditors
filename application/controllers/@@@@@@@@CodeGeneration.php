<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CodeGeneration extends CI_Controller {

	public function index()
	{
        $this->load->model('GenerateCode_model');
        $getPreviousGeneratedCodeList = $this->db->query("SELECT * FROM code_generations WHERE is_active = 1")->result();

	    if($this->session->userdata['role'] == (1 || 2) ){
	        $this->load->view('user/code_generation',array('generatedCode'=>$getPreviousGeneratedCodeList));
	    }else{
	        $this->load->view('basic/login');
	    }
	}
	public function Codeauthorize()
	{

		$this->load->model('GenerateCode_model');
		$getPreviousGeneratedCodeList = $this->db->query("SELECT * FROM code_generations WHERE is_active = 1")->result();

		if($this->session->userdata['role'] == (1 || 2) ){
			$this->load->view('user/code_authorize',array('generatedCode'=>$getPreviousGeneratedCodeList));
		}else{
			$this->load->view('basic/login');
		}
	}

    public function save_code_data(){

		if ($this->input->post('codeGenerationsId')==''){
			$prefix=$_REQUEST['prefix'];
			$getCodeGenerationsData = $this->db->query("SELECT * FROM code_generations WHERE prefix='".$prefix."'")->row();
			if($getCodeGenerationsData){
    			if ($_REQUEST['number_starts_form']<$getCodeGenerationsData->number_of_code_generations){
    				$data=array(
    					'support'=>$getCodeGenerationsData->number_of_code_generations+$getCodeGenerationsData->number_starts_form,
    					'status'=>'error',
    				);
    				print_r(json_encode($data));
    				exit();
    			}
			}
		}

        $this->load->model('GenerateCode_model');

        $data=array(
            "company_name" => $this->input->post('company_name'),
            "customer_name"=>$this->input->post('customer'),
            "order_date"=>$this->input->post('order_date'),
            "delivery_date" =>$this->input->post('delivery_date'),
            "order_no"=>$this->input->post('order_no'),
            "remark"=>$this->input->post('remark'),
            "prefix" => $this->input->post('prefix'),
            "number_of_code_generations" => $this->input->post('number_of_code_generations'),
            "number_starts_form" => $this->input->post('number_starts_form'),
            "is_active"=>1,
            "created_by" =>  $this->session->userdata['user_id'],
            "created_date" => date("Y-m-d h:i:s"));

        if ($this->input->post('codeGenerationsId')) {
			$this->db->where('code_generations_id', $this->input->post('codeGenerationsId'));
			$response=$this->db->update('code_generations', $data);
		}else{
			$response=$this->GenerateCode_model->save_generated_code_data($data);
		}

        if ($response){
        	if ($this->input->post('codeGenerationsId')){
				$lastGenrationsId = $this->input->post('codeGenerationsId');
			}else {
				$lastGenrationsId = $this->db->insert_id();
			}

            $selectedItemArray =isset($_REQUEST['CodeGenerationsItems'])?$_REQUEST['CodeGenerationsItems']:'';
            if ($selectedItemArray) {
                foreach ($selectedItemArray as $key => $item) {
                	if ($item['codeGenerationsItemId']){
						$updateData = array(
							'color_code' => $item['colorCode'],
							'brand_id' => $item['brandName'],
							'item_name' => $item['itemName'],
							'gens_qty' => $item['gensQty'],
							'gens_rate' => $item['gensRate'],
							'ladies_qty' => $item['ladiesQty'],
							'ladies_rate' => $item['ladiesRate'],
							'item_febrics' => $item['febricsItem'],
							'generated_status' => $item['generatedStatus'],
							'is_active' => 1
						);
						$this->db->where('id', $item['codeGenerationsItemId']);
						$this->db->update('code_generations_items', $updateData);
					}else {

						$insertedData = array(
							'code_generations_id' => $lastGenrationsId,
							'color_code' => $item['colorCode'],
							'brand_id' => $item['brandName'],
							'item_name' => $item['itemName'],
							'gens_qty' => $item['gensQty'],
							'gens_rate' => $item['gensRate'],
							'ladies_qty' => $item['ladiesQty'],
							'ladies_rate' => $item['ladiesRate'],
							'item_febrics' => $item['febricsItem'],
							//'patterns_type' => $item['itemName'],
							'generated_status' => $item['generatedStatus'],
							'is_active' => 1
						);
						$this->db->insert('code_generations_items', $insertedData);
					}
                }
            }

            $generatesCodeid=array('genratesCodeId'=>$lastGenrationsId);
            $newData=array_merge($data,$generatesCodeid);
            print_r(json_encode($newData));
            exit();
        }
    }

    public function getCodeGenerationsData(){

	    $echoCodeGenerationsId=$_REQUEST['id'];
	    if ($echoCodeGenerationsId) {
            $getCodeGenerationsData = $this->db->query("SELECT * FROM code_generations WHERE code_generations_id =$echoCodeGenerationsId")->row();
            $codeGenerationsItem = $this->db->query("SELECT * FROM code_generations_items WHERE code_generations_id =$echoCodeGenerationsId")->result();

            $dataList = array(
                'CodeGenerationsData' => json_encode($getCodeGenerationsData),
                'codeGenerationsItem' => json_encode($codeGenerationsItem),
            );

            print_r(json_encode($dataList));
            exit();
        }
    }

    public function get_code_number(){
		$customerName = $this->security->xss_clean(htmlspecialchars(strip_tags($this->input->post('customerName'))));

		$this->db->select();
		$this->db->from('code_generations');
		$this->db->where('customer_name',$customerName);
		$this->db->where('is_active','1');
		$query = $this->db->get();

		if( $query->num_rows() > 0 )
		{
			print_r(json_encode($query->result_array()));
			exit();
		}
		else
		{
			return false;
		}

	}
	
		public function importCode(){

		if($_FILES['import_file']['name']) {
			$this->load->model('GenerateCode_model');

			$startForm = $this->input->post('startUp');
			$dataEndOFCode = $this->input->post('dataEndOFCode');
			$getAuthorizations = $this->db->query("SELECT * FROM code_authorizations WHERE start_from=$startForm and start_end=$dataEndOFCode and prefix='" . $this->input->post('prefix') . "'")->row();
			if ($getAuthorizations) {
				$lastInsertedId = $getAuthorizations->id;
				$start_from = $getAuthorizations->start_from;
				$start_end = $getAuthorizations->start_end;
			} else {
				$data = array(
					"company_name" => $this->input->post('companyName'),
					"customer_name" => $this->input->post('customerName'),
					"prefix" => $this->input->post('prefix'),
					"start_from" => $this->input->post('startUp'),
					"start_end" => $this->input->post('dataEndOFCode'));

				$response = $this->GenerateCode_model->save_authorization_data($data);

				if ($response) {
					$lastInsertedId = $this->db->insert_id();
					$start_from = $this->input->post('startUp');
					$start_end = $this->input->post('dataEndOFCode');

					$endedItem = $start_from + $start_end;
					for ($index = $start_from; $index <= $endedItem; $index++) {
						$insertedData = array(
							'code_authorization_id' => $lastInsertedId,
							'unique_code' => $this->input->post('prefix') . '-' . $index,
							'billing_status' => 0
						);

						$this->GenerateCode_model->save_authorization_details_data($insertedData);
					}

				}
			}

			if ($start_end && $start_from) {

				if ($_FILES['import_file']['name']) {
					$filename = explode(".", $_FILES['import_file']['name']);
					if ($filename[1] == 'csv') {
						$final_array = array();
						$handle = fopen($_FILES['import_file']['tmp_name'], "r");
						$count = 0;
						while ($data = fgetcsv($handle)) {
							if ($count >= 1) {
								$getAuthorizationsDetails = $this->db->query("SELECT * FROM code_authorizations_details WHERE unique_code='" . $data[1] . "'")->row();
								if (!empty($getAuthorizationsDetails) && !empty($data[2])) {
									$updateData = array(
										'unique_name' => $data[2],
										'gender' => $data[3],
										'authorization' => $data[4],
										'roll_no' => $data[5],
										'cell_no' => $data[6],
										'stud_name' => $data[7],
										'mens_tak' => $data[8],
										'mens_taken' => $data[9],
										'billing_status' => 0
									);
									
									$this->db->where('id', $getAuthorizationsDetails->id);
									$response = $this->db->update('code_authorizations_details', $updateData);

								}
							}
							$count++;
						}
						fclose($handle);
					}
				}
			}

			echo 1;
			exit();
		}else{
			echo 2;
			exit();
		}
	}

	public function getGenerationsInfo(){

		$startForm=$this->input->post('startUp');
		$dataEndOFCode=$this->input->post('dataEndOFCode');
		$getAuthorizations = $this->db->query("SELECT * FROM code_authorizations WHERE start_from=$startForm and start_end=$dataEndOFCode and prefix='".$this->input->post('prefix')."'")->row();
		if ($getAuthorizations) {
			$getAllAuthorizationData = $this->db->query("SELECT * FROM code_authorizations_details WHERE code_authorization_id=$getAuthorizations->id")->result();
			print_r(json_encode($getAllAuthorizationData));
			exit();
		}
	}
}
