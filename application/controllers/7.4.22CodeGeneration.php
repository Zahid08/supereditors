<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CodeGeneration extends CI_Controller {

	public function index()
	{
        $this->load->model('GenerateCode_model');
        $getPreviousGeneratedCodeList = $this->db->query("SELECT * FROM code_generations WHERE is_active = 1")->result();

	    if($this->session->userdata['role'] == (1 || 2) ){
	        $this->load->view('user/code_generation',array('generatedCode'=>$getPreviousGeneratedCodeList));
	    }else{
	        $this->load->view('basic/login');
	    }
	}

	public function Codeauthorize()
	{

		$this->load->model('GenerateCode_model');
		$getPreviousGeneratedCodeList = $this->db->query("SELECT * FROM code_generations WHERE is_active = 1")->result();

		if($this->session->userdata['role'] == (1 || 2) ){
			$this->load->view('user/code_authorize',array('generatedCode'=>$getPreviousGeneratedCodeList));
		}else{
			$this->load->view('basic/login');
		}
	}
	
    public function save_code_data(){
        $this->load->model('GenerateCode_model');

        $data=array(
            "company_name" => $this->input->post('company_name'),
            "customer_name"=>$this->input->post('customer'),
            "order_date"=>$this->input->post('order_date'),
            "delivery_date" =>$this->input->post('delivery_date'),
            "order_no"=>$this->input->post('order_no'),
            "remark"=>$this->input->post('remark'),
            "prefix" => $this->input->post('prefix'),
            "number_of_code_generations" => $this->input->post('number_of_code_generations'),
            "number_starts_form" => $this->input->post('number_starts_form'),
            "is_active"=>1,
            "created_by" =>  $this->session->userdata['user_id'],
            "created_date" => date("Y-m-d h:i:s"));

        if ($this->input->post('codeGenerationsId')=='') {
            $prefix = $this->input->post('prefix');
            $checkPrefix = $this->db->query("SELECT * FROM code_generations WHERE prefix='" . $prefix . "'")->row();
            if ($checkPrefix) {
                echo "exit_prefix";
                exit();
            }
        }
        
        if ($this->input->post('codeGenerationsId')) {
			$this->db->where('code_generations_id', $this->input->post('codeGenerationsId'));
			$response=$this->db->update('code_generations', $data);
		}else{
			$response=$this->GenerateCode_model->save_generated_code_data($data);
		}

        if ($response){
        	if ($this->input->post('codeGenerationsId')){
				$lastGenrationsId = $this->input->post('codeGenerationsId');
			}else {
				$lastGenrationsId = $this->db->insert_id();
			}

            $selectedItemArray =isset($_REQUEST['CodeGenerationsItems'])?$_REQUEST['CodeGenerationsItems']:'';
            if ($selectedItemArray) {
                foreach ($selectedItemArray as $key => $item) {
                	if ($item['codeGenerationsItemId']){
						$updateData = array(
							'item_name' => $item['itemName'],
							'gens_qty' => $item['gensQty'],
							'gens_rate' => $item['gensRate'],
							'ladies_qty' => $item['ladiesQty'],
							'ladies_rate' => $item['ladiesRate'],
							'item_febrics' => $item['febricsItem'],
							'generated_status' => $item['generatedStatus'],
							'is_active' => 1
						);
						$this->db->where('id', $item['codeGenerationsItemId']);
						$this->db->update('code_generations_items', $updateData);
					}else {

						$insertedData = array(
							'code_generations_id' => $lastGenrationsId,
							'item_name' => $item['itemName'],
							'gens_qty' => $item['gensQty'],
							'gens_rate' => $item['gensRate'],
							'ladies_qty' => $item['ladiesQty'],
							'ladies_rate' => $item['ladiesRate'],
							'item_febrics' => $item['febricsItem'],
							//'patterns_type' => $item['itemName'],
							'generated_status' => $item['generatedStatus'],
							'is_active' => 1
						);
						$this->db->insert('code_generations_items', $insertedData);
					}
                }
            }

            $generatesCodeid=array('genratesCodeId'=>$lastGenrationsId);
            $newData=array_merge($data,$generatesCodeid);
            print_r(json_encode($newData));
            exit();
        }
    }

    public function getCodeGenerationsData(){

	    $echoCodeGenerationsId=$_REQUEST['id'];
	    if ($echoCodeGenerationsId) {
            $getCodeGenerationsData = $this->db->query("SELECT * FROM code_generations WHERE code_generations_id =$echoCodeGenerationsId")->row();
            $codeGenerationsItem = $this->db->query("SELECT * FROM code_generations_items WHERE code_generations_id =$echoCodeGenerationsId")->result();

            $dataList = array(
                'CodeGenerationsData' => json_encode($getCodeGenerationsData),
                'codeGenerationsItem' => json_encode($codeGenerationsItem),
            );

            print_r(json_encode($dataList));
            exit();
        }
    }
    
        public function get_code_number(){
		$customerName = $this->security->xss_clean(htmlspecialchars(strip_tags($this->input->post('customerName'))));

		$this->db->select();
		$this->db->from('code_generations');
		$this->db->where('customer_name',$customerName);
		$this->db->where('is_active','1');
		$query = $this->db->get();

		if( $query->num_rows() > 0 )
		{
			print_r(json_encode($query->result_array()));
			exit();
		}
		else
		{
			return false;
		}

	}
}
